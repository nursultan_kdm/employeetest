
create table Position (
	Id int identity primary key,
	Position nvarchar(50) not null
)

create table Department (
	Id int identity primary key,
	Department nvarchar(50) not null
)

create table Employee (
	Id int identity primary key,
	FirstName nvarchar(50) not null,
	LastName nvarchar(50) not null,
	PositionId int foreign key references Position(Id),
	DepartmentId int foreign key references Department(Id),
	Birthday datetime
)