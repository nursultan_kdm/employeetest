﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Employee.Meta;
using Employee.Repositories.GenericRepository;

namespace Employee.Controllers
{
    public class EmployeesController : Controller
    {
        private IGenericRepository<Position> positionRepository;
        private IGenericRepository<Department> departmentRepository;
        private IGenericRepository<Employee> employeeRepository;

        public EmployeesController(IGenericRepository<Position> positionRepository, IGenericRepository<Department> departmentRepository, IGenericRepository<Employee> employeeRepository)
        {
            this.positionRepository = positionRepository;
            this.departmentRepository = departmentRepository;
            this.employeeRepository = employeeRepository;
        }

        // GET: Employees
        public ActionResult Index()
        {
            
            return View(employeeRepository.GetAll().ToList());
        }

        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = employeeRepository.GetSingleBy(x => x.Id == id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(departmentRepository.GetAll().ToList(), "Id", "Department1");
            ViewBag.PositionId = new SelectList(positionRepository.GetAll().ToList(), "Id", "Position1");
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,PositionId,DepartmentId,Birthday")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                employeeRepository.Add(employee);
                employeeRepository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.DepartmentId = new SelectList(departmentRepository.GetAll().ToList(), "Id", "Department1");
            ViewBag.PositionId = new SelectList(positionRepository.GetAll().ToList(), "Id", "Position1");
            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = employeeRepository.GetSingleBy(x => x.Id == id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentId = new SelectList(departmentRepository.GetAll().ToList(), "Id", "Department1");
            ViewBag.PositionId = new SelectList(positionRepository.GetAll().ToList(), "Id", "Position1");
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,PositionId,DepartmentId,Birthday")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                employeeRepository.Edit(employee);
                employeeRepository.Save();
                return RedirectToAction("Index");
            }
            ViewBag.DepartmentId = new SelectList(departmentRepository.GetAll().ToList(), "Id", "Department1");
            ViewBag.PositionId = new SelectList(positionRepository.GetAll().ToList(), "Id", "Position1");
            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = employeeRepository.GetSingleBy(x => x.Id == id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = employeeRepository.GetSingleBy(x => x.Id == id);
            employeeRepository.Remove(employee);
            employeeRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                employeeRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
