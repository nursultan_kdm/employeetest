﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Employee.Repositories.GenericRepository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {

        private readonly IContext<T> context;

        public GenericRepository()
        {
            context = new Context<T>();
        }

        public T Add(T entity)
        {
            return context.DbSet.Add(entity);
        }

        public void Dispose()
        {
            context.DbContext.Dispose();
        }

        public void Edit(T entity)
        {
            context.DbSet.Attach(entity);
            context.DbContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public IQueryable<T> GetAll()
        {
            return context.DbSet;
        }

        public IQueryable<T> GetBy(Expression<Func<T, bool>> predicate)
        {
            var result = context.DbSet.Where(predicate);
            return result;
        }

        public T GetSingleBy(Expression<Func<T, bool>> predicate)
        {
            var result = context.DbSet.SingleOrDefault(predicate);
            return result;
        }

        public void Remove(T entity)
        {
            context.DbSet.Remove(entity);
        }

        public void Save()
        {
            context.DbContext.SaveChanges();
        }
    }
}