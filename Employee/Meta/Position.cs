﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Employee.Meta
{
    [MetadataType(typeof(PositionMetaData))]
    public partial class Position
    {
    }

    public class PositionMetaData
    {
        [Display(Name = "Название позиции")]
        [Required]
        public Object Position1 { get; set; }
    }
}