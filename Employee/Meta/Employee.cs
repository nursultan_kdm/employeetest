﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Employee.Meta
{
    public partial class Employee
    {
    }

    public class EmployeeMetaData
    {
        [Display(Name = "Имя")]
        [Required]
        public Object FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        public Object LastName { get; set; }

        [Display(Name = "Дата рождения")]
        public Object Birthday { get; set; }
    }
}